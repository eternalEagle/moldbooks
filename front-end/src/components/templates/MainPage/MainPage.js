import React from "react";

import Header from "../../organisms/Header";

const MainPage = () => {
  return (
    <div className="container">
      <Header />
    </div>
  );
};

export default MainPage;
