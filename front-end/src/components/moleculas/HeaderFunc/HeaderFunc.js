import React from "react";

import HeaderForm from "../HeaderForm/HeaderForm";

const HeaderFunc = () => {
  return (
    <div className="header__func">
      <h2>
        Citește <span>cartea</span> care a <br />
        ales pe tine!
      </h2>
      <HeaderForm />
    </div>
  );
};

export default HeaderFunc;
