import React from "react";

import HeaderLogo from "../../atoms/HeaderLogo/HeaderLogo";
import CartBtn from "../../atoms/CartBtn/CartBtn";
import LoginBtn from "../../atoms/LoginBtn/LoginBtn";

const HeaderTop = () => {
  return (
    <header className="header__top">
      <HeaderLogo />
      <CartBtn />
      <LoginBtn />
    </header>
  );
};

export default HeaderTop;
