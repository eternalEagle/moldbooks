import React, {useState,useCallback} from "react";
import axios from 'axios';

import searchImg from "../../../images/search.svg";

const HeaderForm = () => {
  const [query,setQuery]=useState('');
  const [isLoading,setIsLoading]=useState(false);
  const [results,setResults]=useState([]);

  let prevRequest; // Will cancel  previous requesr if state shanges

  const inputChange = useCallback((e)=>{
    setQuery(e.target.value)
    prevRequest?.cancel();
    prevRequest = axios.CancelToken.source();
    const fetchPost = async () => {
      try {
        const res = await axios.get(`endpointURL`, {
          cancelToken: prevRequest.token,
        })
        setIsLoading(false);
        setResults(res);
      } catch (err) {
        console.log('There was a problem or request was cancelled.')
      }
    }
    fetchPost()
  },[query]);

  return (
    <form className="header__form">
      <input type="text" placeholder="Caută aici" className="header__input" onChange={inputChange}  />
      <button type="submit" className="header__search">
        <span>Search</span>
        <img src={searchImg} alt="*" />
      </button>
    </form>
  );
};

export default HeaderForm;
