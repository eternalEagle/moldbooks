import React from "react";
import logo from "../../../images/header-logo.svg";

const HeaderLogo = () => (
  <div className="header__logo">
    <img src={logo} alt="logo" className="header__img" />
    <h1>Moldova Books</h1>
  </div>
);

export default HeaderLogo;
