import React from "react";

import cartImg from "../../../images/cart.svg";

const CartBtn = () => {
  return (
    <button className="header__cart-btn">
      <img src={cartImg} alt="cart" className="header__cart-img" />
    </button>
  );
};

export default CartBtn;
