import React from "react";

import HeaderTop from "../moleculas/HeaderTop/HeaderTop";
import HeaderFunc from "../moleculas/HeaderFunc/HeaderFunc";

const Header = () => {
  return (
    <header className="header">
      <HeaderTop />
      <HeaderFunc />
    </header>
  );
};

export default Header;
