import React from "react";
import ReactDOM from "react-dom";
//import { Provider } from "react-redux";
//import store from "./store-redux";

import PublicRouter from "./routers/PubicRouter";
import "./components/main.scss";

const App = () => {
  return (
    // <Provider store={store}>
    <PublicRouter />
    // </Provider>
  );
};

ReactDOM.render(<App />, document.querySelector("#app"));
