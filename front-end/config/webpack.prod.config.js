const webpack = require("webpack");
const merge = require("webpack-merge");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const baseConfig = require("./webpack.base.config");
const optimizationConfig = require("./webpack.opt.config");

const productionConfiguration = (env) => {
  const NODE_ENV = env.NODE_ENV ? env.NODE_ENV : "development";
  return {
    devtool: "source-map",
    plugins: [
      new webpack.DefinePlugin({
        "process.env.NODE_ENV": JSON.stringify(NODE_ENV),
      }),
      new ExtractTextPlugin("style.[hash].css"),
    ],
  };
};

module.exports = merge.smart(
  baseConfig,
  optimizationConfig,
  productionConfiguration
);
