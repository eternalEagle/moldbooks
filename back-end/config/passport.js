const JwtStrategy = require("passport-jwt").Strategy,
  ExtractJwt = require("passport-jwt").ExtractJwt;
ObjectId = require("mongoose").Types.ObjectId;

// load up the user model
const User = require("../models/userModel");
const config = require("./mongoose"); // get db config file

module.exports = function (passport) {
  //options setup
  const opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
  opts.secretOrKey = config;

  passport.use(
    new JwtStrategy(opts, function (jwt_payload, done) {
      User.findOne({ _id: new ObjectId(jwt_payload._id) }, function (
        err,
        user
      ) {
        if (err) {
          return done(err, false);
        }
        if (user) {
          done(null, user);
        } else {
          done(null, false);
        }
      });
    })
  );
};
