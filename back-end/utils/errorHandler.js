class ErrorGenerator extends Error {
  constructor(statusCode, message) {
    super();
    this.statusCode = statusCode;
    this.message = message;
  }
}

const handleError = (err, req, res, next) => {
  if (res.headersSent) {
    return next(err);
  }
  res.status(err.statusCode || 500).json({ error: err.message });
};

module.exports = {
  ErrorGenerator,
  handleError,
};
