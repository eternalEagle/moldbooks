var ObjectId = require("mongoose").Types.ObjectId;
const passport = require("passport");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const config = require("../config/mongoose");
require("../config/passport")(passport);

const { ErrorGenerator } = require("../utils/errorHandler");
const User = require("../models/userModel");

exports.getToken = (headers) => {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(" ");
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

exports.handleSignUp = async (req, res, next) => {
  if (!req.body.username || !req.body.password) {
    return next(new ErrorGenerator(401, "Please pass username and password."));
  } else {
    //Checking for existing user in db
    let userAlreadyExists = false;
    try {
      usernameAlreadyExists = await User.findOne({
        username: req.body.username,
      });
      emailAlreadyExists = await User.findOne({ email: req.body.email });
    } catch (err) {
      return next(new ErrorGenerator(500, "Signup went wrong,try again"));
    }
    if (usernameAlreadyExists) {
      return next(new ErrorGenerator(403, "This username already exists"));
    }
    if (emailAlreadyExists) {
      return next(new ErrorGenerator(403, "This email is already registered"));
    }

    const newUser = new User({
      name: req.body.name,
      surname: req.body.surname,
      email: req.body.email,
      username: req.body.username,
      password: await bcrypt.hash(req.body.password, 15),
    });

    try {
      await newUser.save();
    } catch (err) {
      return next(new ErrorGenerator(500, "Signup went wrong,try again"));
    }
    var token = jwt.sign(newUser.toJSON(), config, {
      expiresIn: 604800, // 1 week
    });
    // return the information including token as JSON
    res.status(201).json({ token: "JWT " + token });
  }
};

exports.handleSignIn = async (req, res, next) => {
  let user;
  try {
    user = await User.findOne({
      username: req.body.username,
    });
  } catch (err) {
    return next(new ErrorGenerator(500, "Login failed, please try again"));
  }
  if (!user) {
    return next(
      new ErrorGenerator(401, "Authentication failed. User not found.")
    );
  } else {
    // check if password matches
    const passwordIsValid = await bcrypt.compare(
      req.body.password,
      user.password
    );

    if (!passwordIsValid) {
      return next(
        new ErrorGenerator(401, "Authentication failed. Invalid password.")
      );
    } else {
      // if user is found and password is right create a token
      const token = jwt.sign(user.toJSON(), config, {
        expiresIn: 86400, // 1 week
      });
      // return the information including token as JSON
      res.json({ token: "JWT " + token });
    }
  }
};

exports.handleSignOut = (req, res) => {
  req.logout();
  res.status(204);
};

exports.handleDelete = async (req, res, next) => {
  var token = getToken(req.headers);
  console.log(req.user);
  if (token) {
    try {
      await User.deleteOne({ _id: new ObjectId(req.user._id) });
    } catch {
      return next(new ErrorGenerator(500, "Unable to delete user."));
    }
  } else {
    return next(new ErrorGenerator(403, "Unauthorized."));
  }
};

exports.handleFetchUsersList = async (req, res, next) => {
  let users;
  try {
    users = await User.find({ ...req.criteria }).select(["-password"]);
  } catch (err) {
    console.log(err);
    return next(new ErrorGenerator(500, "Could not find users"));
  }
  res.send(users);
};

exports.handleFindUser = async (req, res, next) => {
  let users;
  try {
    users = await User.find({
      $or: [
        {
          name: {
            $regex: `.*${req.params.search.split(" ")[0]}.*`,
            $options: "i",
          },
        },
        {
          name: {
            $regex: `.*${req.params.search.split(" ")[1]}.*`,
            $options: "i",
          },
        },
        {
          surname: {
            $regex: `.*${req.params.search.split(" ")[0]}.*`,
            $options: "i",
          },
        },
        {
          surname: {
            $regex: `.*${req.params.search.split(" ")[1]}.*`,
            $options: "i",
          },
        },
        {
          username: {
            $regex: `.*${req.params.search}.*`,
            $options: "i",
          },
        },
      ],
    }).select(["-password"]);
  } catch (err) {
    console.log(err);
    return next(new ErrorGenerator(500, "Could not find users"));
  }
  res.send(users);
};

exports.checkAuthorization = (req, res) => {
  var token = getToken(req.headers);
  console.log(req.user);
  if (token) {
    res.status(200).json("Authorized");
  } else {
    return next(new ErrorGenerator(403, "Unauthorized."));
  }
};
