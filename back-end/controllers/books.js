var ObjectId = require("mongoose").Types.ObjectId;
const { ErrorGenerator } = require("../utils/errorHandler");
const Book = require("../models/bookModel");
const User = require("../models/userModel");

const getToken = (headers) => {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(" ");
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

exports.getBook = async (req, res, next) => {
  const bookId = req.params.id;
  let book;

  try {
    book = await Book.findOne({
      _id: new ObjectId(bookId),
    }).populate("uploadedBy", { password: 0 });
  } catch {
    return next(
      new ErrorGenerator(500, "Could not find any book, pleade try again.")
    );
  }

  if (!book) {
    return next(
      new ErrorGenerator(404, "Could not find any book, pleade try again.")
    );
  }

  res.json(book);
};

exports.download = async (req, res, next) => {
  const bookId = req.params.id;
  let book;

  try {
    book = await Book.findOneAndUpdate(
      {
        _id: new ObjectId(bookId),
      },
      { $inc: { downloads: 1 } }
    ).populate("uploadedBy", { password: 0 });
  } catch {
    return next(
      new ErrorGenerator(500, "Could not find any book, pleade try again.")
    );
  }

  if (!book) {
    return next(
      new ErrorGenerator(404, "Could not find any book, pleade try again.")
    );
  }

  res.status(200).send();
};

exports.findByName = async (req, res, next) => {
  const name = req.params.name;
  try {
    users = await Book.find({
      name: {
        $regex: `.*${name}.*`,
        $options: "i",
      },
    });
  } catch (err) {
    return next(new ErrorGenerator(500, "Could not find book"));
  }
  res.send(users);
};

exports.bookList = async (req, res, next) => {
  const page = req.params.page;
  try {
    users = await Book.find({}) //Pagination
      .limit(25)
      .skip(25 * page);
  } catch (err) {
    return next(new ErrorGenerator(500, "Could not find books"));
  }
  res.send(users);
};

exports.downloadRatingList = async (req, res, next) => {
  const page = req.params.page;
  try {
    users = await Book.find({})
      .sort({ downloads: -1 })
      .limit(2)
      .skip(2 * page);
  } catch (err) {
    return next(new ErrorGenerator(500, "Could not find books"));
  }
  res.send(users);
};

exports.ratingList = async (req, res, next) => {
  const page = req.params.page;
  try {
    users = await Book.find({})
      .sort({ rating: -1 })
      .limit(2)
      .skip(2 * page);
  } catch (err) {
    return next(new ErrorGenerator(500, "Could not find books"));
  }
  res.send(users);
};

exports.authenticate = (req, res, next) => {
  //!Authorization check, in case of success all next middleware will be allowed, if not - middleware will be hidden
  var token = getToken(req.headers);
  if (token) {
    next();
  } else {
    return next(new ErrorGenerator(403, "Unauthorized."));
  }
};

exports.myBookList = async (req, res, next) => {
  const userId = req.user._id;
  console.log(111);

  let uploadedBooksList;
  try {
    const { uploadedBooks } = await User.findOne({ _id: userId }).populate(
      "uploadedBooks"
    );
    uploadedBooksList = uploadedBooks;
  } catch {
    return next(new ErrorGenerator(500, "Something wrong, please try again."));
  }

  res.json(uploadedBooksList);
};

exports.favoriteAdd = async (req, res, next) => {
  const userId = req.user._id;
  const bookId = req.params.bookId;

  try {
    const bookExists = await Book.findOne({ _id: new ObjectId(bookId) });
    if (bookExists) {
      await User.findOneAndUpdate(
        { _id: userId, favorites: { $nin: new ObjectId(bookId) } },
        { $push: { favorites: new ObjectId(bookId) } }
      );
    } else {
      return next(
        new ErrorGenerator(500, "Could not find any book, pleade try again.")
      );
    }
  } catch (err) {
    return next(new ErrorGenerator(500, "Something wrong, please try again."));
  }
  res.status(201).json(bookId);
};

exports.favoriteDelete = async (req, res, next) => {
  const userId = req.user._id;
  const bookId = req.params.bookId;

  try {
    await User.findOneAndUpdate(
      { _id: userId, favorites: { $in: new ObjectId(bookId) } },
      { $pull: { favorites: new ObjectId(bookId) } }
    );
  } catch (err) {
    return next(new ErrorGenerator(500, "Something wrong, please try again."));
  }
  res.status(201).json(bookId);
};

exports.postBook = async (req, res, next) => {
  const {
    link,
    avatar,
    name,
    description,
    author,
    language,
    age,
    subject,
    pages,
  } = req.body;

  const uploadedBy = new ObjectId(req.user._id);

  const book = new Book({
    link,
    avatar,
    name,
    author,
    description,
    language,
    age,
    subject,
    pages,
    uploadedBy,
  });

  let newBook;

  const session = await Book.startSession();
  session.startTransaction();

  try {
    const opts = { session };

    newBook = await book.save(opts);
    await User.findOneAndUpdate(
      { _id: book.uploadedBy },
      { $push: { uploadedBooks: newBook._id } },
      opts
    );
    await session.commitTransaction();
    session.endSession();
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    return next(new ErrorGenerator(500, "Something went wrong, try again"));
  }

  res.status(201).json(newBook._id);
};

exports.deleteBook = async (req, res, next) => {
  const bookId = req.params.id;
  let book;

  try {
    book = await Book.findOne({ _id: new ObjectId(bookId) }).populate(
      "uploadedBy"
    );
  } catch {
    return next(
      new ErrorGenerator(500, "Could not find any book, pleade try again.")
    );
  }

  if (!book) {
    return next(
      new ErrorGenerator(404, "Could not find any book, pleade try again.")
    );
  }
  if (book.uploadedBy._id.toString() !== req.user._id.toString()) {
    return next(new ErrorGenerator(405, "You did not upload this book"));
  }
  if (book.protected && !req.user.admin) {
    return next(
      new ErrorGenerator(
        405,
        "This book is protected, contact the administration"
      )
    );
  } else {
    const session = await Book.startSession();
    session.startTransaction();

    try {
      const opts = { session };

      await User.update(
        { _id: book.uploadedBy._id },
        { $pull: { uploadedBooks: new ObjectId(bookId) } },
        opts
      );

      await Book.deleteOne({ _id: new ObjectId(bookId) }, opts);
      await session.commitTransaction();
      session.endSession();
    } catch (err) {
      await session.abortTransaction();
      session.endSession();
      return next(
        new ErrorGenerator(500, "Could not delete the book, please try again.")
      );
    }
    res.status(200).json("Deleted");
  }
};

exports.protectBook = async (req, res, next) => {
  if (req.user.admin) {
    const bookId = req.params.id;
    let book;

    try {
      book = await Book.findOneAndUpdate(
        { _id: new ObjectId(bookId) },
        { protected: true }
      );
    } catch {
      return next(
        new ErrorGenerator(500, "Could not find any book, pleade try again.")
      );
    }

    if (!book) {
      return next(new ErrorGenerator(500, "Book does not exist."));
    }
  } else {
    return next(new ErrorGenerator(403, "You are not admin."));
  }
};

exports.unProtectBook = async (req, res, next) => {
  if (req.user.admin) {
    const bookId = req.params.id;
    let book;

    try {
      book = await Book.findOneAndUpdate(
        { _id: new ObjectId(bookId) },
        { protected: true }
      );
    } catch {
      return next(
        new ErrorGenerator(500, "Could not find any book, pleade try again.")
      );
    }

    if (!book) {
      return next(new ErrorGenerator(500, "Book does not exist."));
    }
  } else {
    return next(new ErrorGenerator(403, "You are not admin."));
  }
};
