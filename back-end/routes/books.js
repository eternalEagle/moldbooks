const router = require("express").Router();
const passport = require("passport");

require("../config/passport")(passport);
const {
  getBook,
  download,
  findByName,
  bookList,
  downloadRatingList,
  ratingList,
  authenticate,
  myBookList,
  favoriteAdd,
  favoriteDelete,
  postBook,
  deleteBook,
  protectBook,
  unProtectBook,
} = require("../controllers/books");

router.get("/get/:id/", getBook);
router.post("/download/:id", download);
router.get("/find/:name", findByName);
router.get("/list/:page", bookList);
router.get("/downloadRate/:page", downloadRatingList);
router.get("/reviewRate/:page", ratingList);
router.use(passport.authenticate("jwt", { session: false }), authenticate);
router.get("/myBooks", myBookList);
router.post("/favoriteAdd/:bookId", favoriteAdd);
router.delete("/favoriteDelete/:bookId", favoriteDelete);
router.post("/post", postBook);
router.delete("/delete/:id", deleteBook);
router.post("/protect/:id", protectBook);
router.post("/unprotect/:id", unProtectBook);

module.exports = router;
