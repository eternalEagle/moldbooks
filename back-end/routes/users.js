const router = require("express").Router();
const passport = require("passport");
require("../config/passport")(passport);

const {
  handleSignUp,
  handleSignIn,
  handleSignOut,
  handleDelete,
  handleFetchUsersList,
  handleFindUser,
  checkAuthorization,
} = require("../controllers/users");

router.post("/signup", handleSignUp);

router.post("/signin", handleSignIn);

router.post(
  "/signout",
  passport.authenticate("jwt", { session: false }),
  handleSignOut
);

router.delete(
  "/delete",
  passport.authenticate("jwt", { session: false }),
  handleDelete
);

router.get("/list", handleFetchUsersList);

router.get("/find/:search", handleFindUser);

router.post(
  "/check",
  passport.authenticate("jwt", { session: false }),
  checkAuthorization
);

module.exports = router;
