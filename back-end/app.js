const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const passport = require("passport");

const mongoURL = require("./config/mongoose");
const usersRouter = require("./routes/users");
const booksRouter = require("./routes/books");
const { handleError } = require("./utils/errorHandler");

const app = express();

app.use(passport.initialize());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors());

//app.use("/login", loginRoutes);

app.use("/users", usersRouter);

app.use("/books", booksRouter);

app.use(handleError);

mongoose
  .connect(mongoURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log("Connected successfull");

    app.listen(3000, () => {
      console.log("Successfull start");
    });
  })
  .catch(() => {
    console.log("Connection failed");
  });
