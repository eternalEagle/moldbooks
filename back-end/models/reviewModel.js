const mongoose = require("mongoose");

const {
  Schema: {
    Types: { String, ObjectId },
  },
} = mongoose;
const { Schema } = mongoose;

const reviewSchema = new Schema({
  postedBy: { type: ObjectId, ref: "User" },
  mark: Number,
  content: String,
  book: { type: ObjectId, ref: "Book" },
});

module.exports = mongoose.model("Review", bookSchema);
