const mongoose = require("mongoose");

const {
  Schema: {
    Types: { String, ObjectId },
  },
} = mongoose;
const { Schema } = mongoose;

const userSchema = new Schema({
  avatar: String,
  name: { type: String, required: true },
  surname: { type: String, required: true },
  email: { type: String, required: true },
  username: { type: String, required: true },
  password: { type: String, required: true },
  uploadedBooks: [{ type: ObjectId, ref: "Book" }],
  favorites: [{ type: ObjectId, ref: "Book" }],
  admin: { type: Boolean, default: false },
  verified: { type: Boolean, default: false },
});

module.exports = mongoose.model("User", userSchema);
