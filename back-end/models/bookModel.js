const mongoose = require("mongoose");

const {
  Schema: {
    Types: { String, ObjectId, Boolean },
  },
} = mongoose;
const { Schema } = mongoose;

const bookSchema = new Schema({
  link: { type: String, required: true },
  avatar: { type: String, required: true },
  name: { type: String, required: true },
  author: [{ type: String, required: true }],
  description: { type: String, required: true },
  language: { type: String, required: true },
  age: { type: String, required: true },
  subject: { type: String, required: true },
  pages: { type: String, required: true },
  rating: { type: Number, default: -1 },
  reviews: [{ type: ObjectId, ref: "Review" }],
  uploadedBy: { type: ObjectId, ref: "User", required: true },
  protected: { type: Boolean, default: false },
  downloads: { type: Number, default: 0 },
});

module.exports = mongoose.model("Book", bookSchema);
