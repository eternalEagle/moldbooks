const element = document.querySelector(".main__select__progress");
const resizers = document.querySelectorAll(".main__select__progress--resizer");
const minValue = document.querySelector(".main__value--min");
const maxValue = document.querySelector(".main__value--max");
for (let i = 0; i < resizers.length; i++) {
  const currentResizer = resizers[i];

  let resizing = false;

  const handleMouseOut = () => {
    resizing = false;
    firstTime = false;
    console.log("Set resizing to ", resizing);
  };

  currentResizer.addEventListener("mousedown", function () {
    resizing = true;
    firstTime = true;
    currentResizer.addEventListener("mouseout", handleMouseOut);
  });

  currentResizer.addEventListener("mouseup", function () {
    resizing = false;
    currentResizer.removeEventListener("mouseout", handleMouseOut);
  });

  currentResizer.addEventListener("mousemove", (e) => {
    if (resizing) {
      resize(e);
    }
  });

  let marginRight = element.style.marginRight.split("p")[0] || 0;
  let marginLeft = element.style.marginLeft.split("p")[0] || 0;

  let distance =
    window.getComputedStyle(element, null).width.split("p")[0] - 9 * 2;

  function resize(e) {
    const movement = e.movementX;
    console.log(movement);
    if (distance >= 0) {
      if (currentResizer.classList.contains("right")) {
        marginRight -= marginRight >= 0 ? movement : -2;
        element.style.marginRight = marginRight + "px";
        maxValue.innerHTML = Math.floor((250 - marginRight) * 4) + "$";
      }
      if (currentResizer.classList.contains("left")) {
        marginLeft += marginLeft >= 0 ? movement : +2;
        element.style.marginLeft = marginLeft + "px";
        minValue.innerHTML = 1000 - Math.floor((250 - marginLeft) * 4) + "$";
      }
      distance =
        window.getComputedStyle(element, null).width.split("p")[0] - 9 * 2;
    }
  }
}
